import tables

type
  MyObj = ref object
    the_str: string

var the_table = newTable[string, MyObj]()

the_table["one"] = nil

the_table["two"] = MyObj(the_str:"Hello")


echo the_table.contains("two")
echo the_table["two"].the_str
echo the_table.contains("one")
echo the_table["one"].the_str
