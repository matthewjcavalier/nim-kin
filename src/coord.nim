import strformat
type Coord* = ref object
  x*: Natural
  y*: Natural

proc `==`*(c1, c2: Coord): bool = c1.x == c2.x and c1.y == c2.y
method to_string*(this:Coord): string = fmt"x:{this.x},y:{this.y}"
proc clone*(coord: Coord): Coord = Coord(x: coord.x, y: coord.y)

proc up*(coord: Coord): Coord = Coord(x: coord.x, y: coord.y - 1)
proc down*(coord: Coord): Coord = Coord(x: coord.x, y: coord.y + 1)
proc left*(coord: Coord): Coord = Coord(x: coord.x - 1, y: coord.y)
proc right*(coord: Coord): Coord = Coord(x: coord.x + 1, y: coord.y)
