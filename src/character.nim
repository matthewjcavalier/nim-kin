import settings
import draw

type Character* = ref object of RootObj
  glyph: string

method get_glyph*(this:Character): string {.base.} = this.glyph
method take_turn*(this:Character){.base.} = exitProc()

type Player_Character = ref object of Character

#method move(this:Player_Character) = 

#method take_turn*(this:Player_Character) =
#  var key_pressed = read_input()
#  case key_pressed:
#    of Key.Up, Key.Down, Key.Left, Key.Right:
#      

proc make_pc*(settings: Settings):Player_Character =
  Player_Character(glyph: "@")




