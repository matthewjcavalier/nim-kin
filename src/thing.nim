import illwill
import os, strutils

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)


illwillInit(fullscreen=true)
# State what to do when getting a Ctrl-c ?
setControlCHook(exitProc)
hideCursor()

var tb = newTerminalBuffer(terminalWidth(), terminalHeight())

tb.setForegroundColor(fgCyan, true)
tb.drawRect(0, 0, 40, 5)
tb.drawHorizLine(2, 38, 3, doubleStyle=true)

#var x = parseInt("8")
#var y = parseInt("9")

var x = 9
var y = 9

tb.write(x, y, fgWhite, "Press any key to display its name")
tb.write(2, 2, "Press ", fgYellow, "ESC", fgWhite, " or ", fgYellow,"Q",
         fgWhite, " to quit")

while true:
  var key = getKey()
  case key
  of Key.None: discard
  of Key.Escape, Key.Q: exitProc()
  else:
    tb.write(8,4, ' '.repeat(31))
    tb.write(2,4,resetStyle, "Key pressed: ", fgGreen, $key)

  tb.display()
  sleep(20)
