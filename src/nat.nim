import math
import rdstdin, strutils


proc howToUse(i: Natural) = echo i

howToUse 50         # Compile
howToUse 150      # Will not compile, try it

 
let num = parseInt(readLineFromStdin "Input a string: ")

howToUse(num)
