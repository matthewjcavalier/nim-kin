import coord

type
  Dataframe*[T] = ref object
    frame*: seq[seq[T]]

method get_data*[T](this: Dataframe[T], x: int, y: int): T {.base.} =
  this.frame[y][x]

method get_data*[T](this: Dataframe[T], coord: Coord): T {.base.} =
  this.get_data(coord.x, coord.y)

method set_data*[T](this: Dataframe[T], x: int, y: int, new_data: T) {.base.} =
  this.frame[y][x] = new_data

method set_data*[T](this: Dataframe[T], coord: Coord, new_data: T) {.base.} =
  this.set_data(coord.x, coord.y, new_data)

method get_rows*[T](this: Dataframe[T]): int {.base.} = this.frame.len

method get_cols*[T](this: Dataframe[T]): int {.base.} =
  if this.get_rows() > 0:
    result = this.frame[0].len
  else:
    result = 0

iterator rows*[T](this: Dataframe[T]): seq[T] =
  var i = 0
  while i < this.get_rows():
    yield this.frame[i]
    inc i

iterator cols*[T](row: seq[T]): T =
  var i = 0
  while i < row.len:
    yield row[i]
    inc i


proc make_frame*[T](height: int, width: int, gen_proc: proc(): T): Dataframe[T] =
  var frame_seq = newSeq[seq[T]](height)

  for row_index in 0..height - 1:
    var row = newSeq[T](width)
    for col_index in 0..width - 1:
      row[col_index] = gen_proc()

    frame_seq[row_index] = row
  
  result = Dataframe[T](frame: frame_seq)
