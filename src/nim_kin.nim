import settings
import dun_gen
import draw
import os
import dataframe
import entity

proc main()=
  let settings = read_conf_file("settings.json")
  let dun = create_dungeon(settings)
  let entity_map = make_frame(settings.max_row, settings.max_col, func():Entity = nil)

  let maps = Maps(dun: dun, entityMap: entity_map)
  var game_running:bool = true

  var io = scr_setup()
  io.draw_whole_dun(dun)

  var pc_loc = dun.get_rand_open_tile(settings)
  let pc = make_pc(settings, io, pc_loc, maps)
  entity_map.set_data(pc_loc, pc)

  io.update_point(pc_loc, pc.get_symbol())

  io.show_changes()

  while game_running:
    pc.take_turn()
    io.show_changes()
    sleep(20)

main()
