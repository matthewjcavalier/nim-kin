import os
import settings
import dataframe
import draw
from illwill_unsafe import Key
import dun_gen
import coord
import dun_gen

type MethodNotImplemented = object of Exception

type Entity* = ref object of RootObj
  symbol: string
  dunIO: DunIO
  curr_pos: Coord

method get_symbol*(this:Entity): string {.base.} = this.symbol

method get_pos*(this:Entity): Coord {.base.} = this.curr_pos

type Maps* = ref object of RootObj
  dun*: Dungeon
  entityMap*: Dataframe[Entity]


type Character* = ref object of Entity
  health: int
  name: string
  io: DunIo
  maps: Maps


method take_turn*(this: Character) {.base.} = raise newException(MethodNotImplemented, "Method needs to be implemnted")

func get_health*(character:Character):int =
  character.health

func get_name*(character:Character): string =
  character.name

method move(this:Character, new_pos:Coord) {.base.} =
  # erase where they are
  this.io.update_point(this.curr_pos, " ")
  this.maps.entityMap.set_data(this.curr_pos, nil)
 
  # set where they will be
  this.curr_pos = new_pos
  this.io.update_point(this.curr_pos, this.get_symbol())
  this.maps.entityMap.set_data(this.curr_pos, this)

method attempt_to_move_to(this:Character, new_pos:Coord): bool {.base.} =
  var nothing_in_the_way:bool = true
  # check if there is anything in the way in the dungeon
  nothing_in_the_way = (0 == this.maps.dun.get_tile(new_pos).hardness)

  # check if there are any entities in the way
  nothing_in_the_way = nothing_in_the_way and (this.maps.entityMap.get_data(new_pos) == nil)

  if nothing_in_the_way:
    this.move(new_pos)

  return nothing_in_the_way


type PlayerCharacter* = ref object of Character


method take_turn*(this: PlayerCharacter) =
  # do the do and get user input
  var pressed = this.io.read_input()
  case pressed
    of Key.Up:
      this.io.update_point(0, this.maps.dun.height, "Pressed Up    ")
      discard this.attempt_to_move_to(this.curr_pos.up)
    of Key.Down:
      this.io.update_point(0, this.maps.dun.height, "Pressed Down  ")
      discard this.attempt_to_move_to(this.curr_pos.down)
    of Key.Left:
      this.io.update_point(0, this.maps.dun.height, "Pressed Left  ")
      discard this.attempt_to_move_to(this.curr_pos.left)
    of Key.Right:
      this.io.update_point(0, this.maps.dun.height, "Pressed Right ")
      discard this.attempt_to_move_to(this.curr_pos.right)
    else:
      this.io.update_point(0, this.maps.dun.height, "Un Allocated")



proc make_pc*(): PlayerCharacter =
  PlayerCharacter(symbol: "@", health: 10, name: "DerArzt")

proc make_pc*(settings:Settings, dunIO: DunIO, curr_pos:Coord, maps: Maps): PlayerCharacter =
  PlayerCharacter(symbol: settings.player_symbol,
  health: settings.player_health,
  name: settings.player_name,
  io: dunIO,
  curr_pos: curr_pos,
  maps: maps)
