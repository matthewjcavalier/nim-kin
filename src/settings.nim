import json
import random
import times

type Settings* = ref object of RootObj
  max_row*: int
  max_col*: int
  max_hard*: int
  min_room_h*: int
  min_room_w*: int
  room_gen_attempts*: int
  rand_gen*: Rand
  max_boulder*: int
  player_health*: int
  player_name*: string
  player_symbol*: string

method rand*(this: Settings, max_val: int): int {.base.} = this.rand_gen.rand(max_val)

func seed_or_curr_time(in_seed: int): int =
  if in_seed == 0:
    return 1
  else:
    return in_seed

proc read_conf_file*(loc:string): Settings =
  let f = open(loc)
  defer: f.close()

  let jsonNode = parseJson(readAll(f))


  var rand_gen = initRand(5157108739)

  Settings(max_row: jsonNode["max-row"].getInt,
           max_col: jsonNode["max-col"].getInt,
           max_hard: jsonNode["max-hard"].getInt,
           min_room_h: jsonNode["min-r-h"].getInt,
           min_room_w: jsonNode["min-r-w"].getInt,
           room_gen_attempts: jsonNode["room-gen-attempts"].getInt,
           player_health: jsonNode["player-health"].getInt,
           player_symbol: jsonNode["player-symbol"].getStr,
           player_name: jsonNode["player-name"].getStr,
           rand_gen: initRand(seed_or_curr_time(jsonNode["seed"].getInt)),
           max_boulder: jsonNode["max-boulders"].getInt)

