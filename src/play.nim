
type myObj = ref object of RootObj
  some:string

var something:myObj = nil

echo "first time:"
echo (something == nil)
echo "second time:"
echo (something != nil)
echo "third time:"
something = myObj(some: "String")
echo (something != nil)
