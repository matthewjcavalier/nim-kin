import settings
import dataframe
import math
import system
import heapqueue
import tables
import sets
import coord
import tile
import room
import strformat

type Dungeon* = ref object
  level*: Dataframe[Tile]
  rooms*: seq[Room]
method height*(this: Dungeon): int {.base.} =
  # Get the number of rows that are in the dungeon
  this.level.get_rows()
method width*(this: Dungeon): int {.base.} =
  # Get the number of cols that are in the dungeon
  this.level.get_cols()

method get_tile*(this:Dungeon, coord:Coord): Tile {.base.} =
  this.level.get_data(coord)

method get_rand_open_tile*(this: Dungeon, settings: Settings): Coord {.base.} =
  # Get the coords of a random tile in the dungeon that is open

  var open_spaces:seq[Coord] = @[]
  var counter = 0
  let total = settings.max_row * settings.max_col
  for row in 0..this.level.get_rows() - 1:
    for col in 0..this.level.get_cols() - 1:
      var tile = this.level.get_data(col, row)
      counter.inc()
      if tile.hardness == 0:
        open_spaces.add(Coord(x:col, y:row))
  var length = open_spaces.len

  return open_spaces[settings.rand(length - 1)]

proc randomize_hardnesses(dun: var Dungeon, settings: Settings) =
  ## Takes in a dungeon object and randomizes the hardness values in all of the
  ## tiles based on the input settings
  for row in dun.level.rows():
    for tile in cols(row):
      tile.set_hardness(settings.rand(settings.max_hard - 1) + 1)

func make_tile(): Tile =
  # Create a new tile object with the defaults of hardness = 1 and type = Wall
  Tile(hardness: 1, t_type: Tile_type.WALL)


proc room_fits(new_room: Room, room_frame: Dataframe[bool]): bool =
  # Check to see if the input Room both fits in the data frame and has
  # no overlap with any of the rooms already in the frame with a 1 tile
  # buffer around each room
  var fits_in_frame: bool = true
  var doesnt_overlap: bool = true

  # room is within dun limits
  fits_in_frame = (new_room.bottom_left.y < room_frame.get_rows - 1 and 
                   new_room.top_right.x < room_frame.get_cols - 1 and 
                   new_room.coord.x > 0 and
                   new_room.coord.y > 0)

  # room doesn't have overlap with other rooms
  if fits_in_frame:
    for x in new_room.coord.x - 1..new_room.top_right.x + 1:
      for y in new_room.coord.y - 1..new_room.bottom_left.y + 1:
        doesnt_overlap = doesnt_overlap and not room_frame.get_data(x, y)
  
  result = fits_in_frame and doesnt_overlap

proc add_rooms(dun: var Dungeon, settings: Settings) =
  # Fill the input dungeon with rooms that don't have overlap
  let max_attempts = settings.room_gen_attempts
  # var so that the list can be appended to
  var rooms: seq[Room] = @[]
  let room_frame: Dataframe[bool] = make_frame(dun.height, dun.width, func(): bool = false)

  for i in 0..max_attempts:
    var new_room: Room =
      Room(coord: Coord(x: settings.rand(settings.max_col), y: settings.rand(settings.max_row)),
           height: settings.min_room_h + settings.rand(floorDiv(settings.max_row, 10)),
           width: settings.min_room_w + settings.rand(floorDiv(settings.max_col, 10)))

    if room_fits(new_room, room_frame):
      # add room to list
      rooms.add(new_room)

      # update the room frame
      for x in new_room.coord.x..new_room.top_right.x:
        for y in new_room.coord.y..new_room.bottom_left.y:
          room_frame.set_data(x, y, true)

  dun.rooms = rooms

  for row in 0..room_frame.get_rows - 1:
    for col in 0..room_frame.get_cols - 1:
      if(room_frame.get_data(col, row)):
        dun.level.set_data(col, row, Tile(hardness: 0, t_type: Tile_type.ROOM))

func pythag(c1: Coord, c2: Coord): int =
  # find the distance between two coords using the Pythagorean theorem
  let x_val: float = math.pow(float(system.abs(c1.x - c2.x)), 2)
  let y_val: float = math.pow(float(system.abs(c1.y - c2.y)), 2)
  int(round(sqrt(x_val + y_val)))


func valid_or_nil(coord: Coord, floor: Dataframe[Tile]): Coord =
  if(coord.x > 0 and
     coord.y > 0 and
     coord.x < floor.get_cols and
     coord.y < floor.get_rows):
    result = coord
  else:
    result = nil


proc coords_around(coord: Coord, floor_frame: Dataframe[Tile]): Table[string,Coord] =
  # create a table for each coord around the current one
  var coord_table = initTable[string,Coord]()
  coord_table.add("up",valid_or_nil(Coord(x: coord.x, y: coord. y - 1), floor_frame))
  coord_table.add("down", valid_or_nil(Coord(x: coord.x, y: coord. y + 1), floor_frame))
  coord_table.add("left", valid_or_nil(Coord(x: coord.x - 1, y: coord. y), floor_frame))
  coord_table.add("right", valid_or_nil(Coord(x: coord.x + 1, y: coord. y), floor_frame))
  result = coord_table


type QueueItem = object
  priority: int
  data: Coord
proc `<`(a,b: QueueItem): bool = a.priority < b.priority


proc work_on_coord(h_fun: proc(coord:Coord, dist:int):int, dist:int, coord: Coord, floor_frame: Dataframe[Tile], dist_frame: Dataframe[int], visited: var HashSet[string], visit_queue: var HeapQueue[QueueItem]) =
  var new_dist: int = dist + floor_frame.get_data(coord).hardness + 1
  var curr_dist: int = dist_frame.get_data(coord)

  if new_dist < curr_dist:
    dist_frame.set_data(coord, new_dist)
  if not visited.contains(coord.to_string()):
    visited.incl(coord.to_string())
    visit_queue.push(QueueItem(priority: h_fun(coord, dist_frame.get_data(coord)), data: coord))


proc create_dist_map(start_p: Coord, end_p: Coord, floor_frame: Dataframe[Tile]): Dataframe[int] =
  # Create a distance map from the start to the end returning a map of ints
  let dist_map = make_frame(floor_frame.get_rows, floor_frame.get_cols, func(): int = 1000000)
  let h_fun = func(coord:Coord, dist: int): int = dist + pythag(end_p, coord)
  var queue = initHeapQueue[QueueItem]()
  var visited: HashSet[string]
  var found_end: bool = false
  let add_to_visited = proc(coord: Coord) = visited.incl(coord.to_string())

  dist_map.set_data(start_p.x, start_p.y, 0)
  #visited.incl("x:{start_p.x},y:{start_p.y}")
  start_p.add_to_visited()
  queue.push(QueueItem(priority: 1, data: start_p))

  while queue.len > 0 and not found_end:
    var curr_coord = queue.pop().data
    found_end = curr_coord == end_p
    if not found_end:
      var curr_dist: int = dist_map.get_data(curr_coord)

      var neighbors:Table[string,Coord] = coords_around(curr_coord, floor_frame)

      if not isNil(neighbors["up"]):
        work_on_coord(h_fun, curr_dist, neighbors["up"], floor_frame, dist_map, visited, queue)
      if not isNil(neighbors["down"]):
        work_on_coord(h_fun, curr_dist, neighbors["down"], floor_frame, dist_map, visited, queue)
      if not isNil(neighbors["left"]):
        work_on_coord(h_fun, curr_dist, neighbors["left"], floor_frame, dist_map, visited, queue)
      if not isNil(neighbors["right"]):
        work_on_coord(h_fun, curr_dist, neighbors["right"], floor_frame, dist_map, visited, queue)

  dist_map
 

func find_next_coord(f_frame: Dataframe[int], coord: Coord): Coord =
  var queue = initHeapQueue[QueueItem]()
  queue.push(QueueItem(priority: f_frame.get_data(coord.up), data: coord.up))
  queue.push(QueueItem(priority: f_frame.get_data(coord.down), data: coord.down))
  queue.push(QueueItem(priority: f_frame.get_data(coord.left), data: coord.left))
  queue.push(QueueItem(priority: f_frame.get_data(coord.right), data: coord.right))
  queue.pop().data


proc gen_path(start_point: Coord, end_point: Coord, floor_frame: Dataframe[Tile]): seq[Coord] =
  # Find the shortest path from start to end allowing for tunneling
  var dist_map: Dataframe[int] = create_dist_map(start_point, end_point, floor_frame)
  var curr_point = end_point
  var path: seq[Coord] = @[]

  path.add(curr_point)

  while not (curr_point == start_point):
    var next_point = find_next_coord(dist_map, curr_point)
    path.add(next_point)
    curr_point = next_point

  path

  

proc link_rooms(r1: Room, r2: Room, floor_frame: Dataframe[Tile]) =
  # create a path between two rooms
  let shortest_path: seq[Coord] = gen_path(r1.coord, r2.coord, floor_frame)
  for coord in shortest_path:
    floor_frame.get_data(coord).set_hardness(0)
    floor_frame.get_data(coord).set_type(Tile_type.HALL)

proc add_halls(dun: Dungeon) =
  # add hallways to connect all of the rooms in the dungeon
  var curr_room: Room = dun.rooms[0]

  for i in 1..dun.rooms.len - 1:
    link_rooms(curr_room, dun.rooms[i], dun.level)
    curr_room = dun.rooms[i]


proc add_boulders(dun:Dungeon, settings: Settings) =
  for i in 0..settings.max_boulder:
    var open_coord = dun.get_rand_open_tile(settings)
    dun.level.get_data(open_coord).set_type(Tile_type.BOULDER)
    dun.level.get_data(open_coord).set_hardness(settings.rand(settings.max_hard))
  

proc create_dungeon*(settings: Settings): Dungeon =
  var new_dun:Dungeon = Dungeon(level: make_frame(settings.max_row, settings.max_col, make_tile))
  randomize_hardnesses(new_dun, settings)
  add_rooms(new_dun, settings)
  add_boulders(new_dun, settings)
  add_halls(new_dun)
  result = new_dun
