import dataframe
from dun_gen import Dungeon
import tile
import illwill_unsafe
import coord

var height:int
var width:int


proc draw_hardness_map*(dun: Dungeon) =
  var str: string = " "
  var str2: string = " "
  for i in 0..dun.level.get_cols():
    str.addInt(i mod 10)
    if(i mod 10) == 0:
      str2.add(i / 10)
    else:
      str2.add(" ")
  echo str2
  echo str
  var counter = 0
  for row in dun.level.rows():
    var row_str:string = ""
    row_str.addInt(counter mod 10)
    for cell in cols(row):
      row_str.add(cell.hardness mod 10)
    row_str.add(counter mod 10)
    echo row_str
    counter += 1
  echo str
  echo str2

func get_symbol(tile: Tile): string =
  case tile.t_type:
    of Tile_type.BOULDER:
      result = "\u2721"
    else:
#      result = "\uffe3"
      result = " "

func get_bg(tile:Tile):BackgroundColor =
  case tile.t_type:
    of Tile_type.WALL:
      result = bgBlack
    else:
      result = bgWhite

func get_fg(tile:Tile):ForeGroundColor =
  case tile.t_type:
    of Tile_type.Wall, Tile_type.BOULDER:
      result = fgBlack
    of Tile_type.Hall, Tile_type.ROOM:
      result = fgWhite


proc draw_map*(dun: Dungeon) =
  for row in dun.level.rows():
    var row_str = ""
    for cell in cols(row):
      row_str.add(get_symbol(cell))
    echo row_str

type DunIO* = ref object of RootObj
  tb:TerminalBuffer

proc exitProc*() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc scr_setup*():DunIO =
  illwillInit(fullscreen=true)
  # State what to do when getting a Ctrl-c ?
  setControlCHook(exitProc)
  hideCursor()
  DunIO(tb: newTerminalBuffer(terminalWidth(), terminalHeight()))


proc draw_whole_dun*(io:DunIO, dun: Dungeon)=
  for row in 0..dun.level.get_rows() - 1:
    for col in 0..dun.level.get_cols() - 1:
      var tile = dun.level.get_data(col, row)
      io.tb.write(col, row, get_fg(tile), get_bg(tile), get_symbol(tile))

  io.tb.display()

proc update_point*(io:DunIO, x,y:int, words:string)=
  io.tb.write(x,y,bg_white, fg_black,words)

proc update_point*(io:DunIO, coord: Coord, words: string)=
  update_point(io, coord.x, coord.y, words)

proc show_changes*(io:DunIO) = io.tb.display()


proc read_input*(io:DunIO):Key =
  while true:
    var key = getKey()
    case key
      of Key.None: discard
      of Key.Escape, Key.Q: exitProc()
      else:
        return key
