# Package

version       = "0.1.0"
author        = "Matt Cavalier"
description   = "A rouge-like dungeon crawler"
license       = "MIT"
srcDir        = "src"
bin           = @["nim_kin"]



# Dependencies

requires "nim >= 1.0.0", "illwill_unsafe"
