
type Tile_type* {.pure.} = enum
  WALL,
  HALL,
  ROOM

type Tile* = ref object
  hardness*: int
  t_type*: Tile_type

method set_hardness*(this: Tile, new_hardness: int) {.base.} =
  # Update the hardness of the tile
  this.hardness = new_hardness
method set_type*(this: Tile, new_type: Tile_type) {.base.} =
  # Update the type of the tile
  this.t_type = new_type

