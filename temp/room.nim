import coord

type Room* = ref object
  coord*: Coord
  height*: int
  width*: int

method bottom_left*(this: Room): Coord {.base.} =
  # Get the coordinate of the bottom left corner of the room
  Coord(x: this.coord.x, y: this.coord.y + this.height)
method bottom_right*(this: Room): Coord {.base.} =
  # Get the coordinate of the bottom right corner of the room
  Coord(x: this.coord.x + this.width, y: this.coord.y + this.height)
method top_left*(this: Room): Coord {.base.} =
  # Get the coordinate of the top left corner of the room
  this.coord
method top_right*(this: Room): Coord {.base.} =
  # Get the coordinate of the top right corner of the room
  Coord(x: this.coord.x + this.coord.y)
  
