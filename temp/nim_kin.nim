from settings import Settings, read_conf_file
import dun_gen
import draw

proc main()=
  let settings = read_conf_file("settings.json")
  let dun = create_dungeon(settings)
  draw_map(dun)

main()
