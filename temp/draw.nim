import dataframe
from dun_gen import Dungeon
import tile


proc draw_hardness_map*(dun: Dungeon) =
  var str: string = " "
  var str2: string = " "
  for i in 0..dun.level.get_cols():
    str.add(i mod 10)
    if(i mod 10) == 0:
      str2.add(i / 10)
    else:
      str2.add(" ")
  echo str2
  echo str
  var counter = 0
  for row in dun.level.rows():
    var row_str:string = ""
    row_str.add(counter mod 10)
    for cell in cols(row):
      row_str.add(cell.hardness mod 10)
    row_str.add(counter mod 10)
    echo row_str
    counter += 1
  echo str
  echo str2

func get_symbol(tile: Tile): char =
  case tile.t_type:
    of Tile_type.WALL:
      result = '#'
    else:
      result = ' '

proc draw_map*(dun: Dungeon) =
  for row in dun.level.rows():
    var row_str = ""
    for cell in cols(row):
      row_str.add(get_symbol(cell))
    echo row_str


